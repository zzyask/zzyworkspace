
#ifdef __MYSTRING__
#define __MYSTRING__

#include <iostream>
#include <>

using namespace std;

 class MyString{
 public:
     //big three 拷贝构造，拷贝赋值，析构函数
     MyString(const char * cstr = 0);
     MyString(const MyString& mstr);
     MyString& operator=(const MyString& mstr);
     ~MyString();

     inline char* get_c_str() const {return m_data};
     inline Mystring
private:
     char * m_data;
 };

MyString::Mystring(ocnst char* cstr = 0){
    if(cstr){
        m+data = new char[(strlen(cstr) + 1)];
        strcpy(m_data,cstr);
    }else{
        m_data = new char[1];
        *m_data = '\0';
    }
}

MyString::~MyString(){
    //array new 要用array delete,不然会导致编译器不知道其是数组，只释放数组下标为0的内存，会导致内存泄漏
    delete[] m_data;
}

MyString::MyString(const MyString& mstr){
    m_data = new char[strlen(mstr.m_data) + 1];
    strcpy(m_data,mstr.m_data);
}

MyString& operator=(const MyString& mstr){

    if(*this == mstr){
        return *this;
    }

    delete[] m_data;
    m_data = new char[ strlen(mstr.data) + 1 ];
    strcpy(m_data,nstr.m_data);
    return *this;
}

ostream& operator<<(ostream& os,const MyString& mstr){
    os << mstr.get_c_str() << endl;
}

int main(){
    return 0;
}

#endif
