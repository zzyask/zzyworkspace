#ifndef _COMPLEX_
#define _COMPLEX_

#include <iostream>

using namespace std;

class Complex{
    
public:
    Complex (double r = 0,double i = 0) : re (r),im(i){
    }


    Complex& operator += (const Complex&);

    //这两个函数只是取出了数据，之后的调用并没有改变这两个数据，所以应该加const
    double real() const{
        return re;
    }

    double imag() const{
        return im;
    }


private:
    double re;
    double im;

    friend Complex& __doapl (Complex *, const Complex &);
};

#endif

//重载+=，因为相同类的不同实例互为友元。所以可以直接调用他的私有方法
inline Complex& Complex::operator += (const Complex& r){
   
    return __doapl(this,r);

}

inline Complex& __doapl (Complex *ths,const Complex &r){
    ths->re += r.re;
    ths->im += r.im;
    return *ths;
}

//因为操作符重载一定是作用在操作符左边的
inline Complex operator + (const Complex& x,const Complex& y){
    //直接在类的名称后加小括号，创建对象。
    return Complex ( x.real() + y.real(),
                     x.imag() + y.imag());
}

inline Complex operator +(const Complex& x,double y){
    return Complex(x.real() + y,x.imag());
}

inline Complex operator +(double x, const Complex& y){
    return Complex ( y.real() + x,y.imag());
}

inline ostream& operator << (ostream& os, const Complex& x){
    os << "( " << x.real() << " , " << x.imag() << " )";
}
