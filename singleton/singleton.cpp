#ifdef __SINGLETON__
#define __SINGLETON__

//单例模式

class A{
    public:
        static A& getInstance(return a;);
        setup() {};

    private:
        A();
        A(const A& rhs);
};

A& A::getInstance(){
    Static A a;
    return a;
}

#endif